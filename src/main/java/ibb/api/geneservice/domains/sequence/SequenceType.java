package ibb.api.geneservice.domains.sequence;

public enum SequenceType {
    TRANSCRIPT, CDS, PROTEIN
}
