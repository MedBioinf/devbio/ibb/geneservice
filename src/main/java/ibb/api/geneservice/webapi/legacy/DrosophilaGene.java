package ibb.api.geneservice.webapi.legacy;

public class DrosophilaGene {
    public String id;
    public String fullname;
    public String symbol;
    public String annotationId;
}
