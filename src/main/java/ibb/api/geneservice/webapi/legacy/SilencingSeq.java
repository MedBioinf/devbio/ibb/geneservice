package ibb.api.geneservice.webapi.legacy;

import java.util.List;

public class SilencingSeq {
    public String id;
    public List<String> geneIds;
    public String leftPrimer;
    public String rightPrimer;
    public String seq;
}
